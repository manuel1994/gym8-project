//dependencies
var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var User = require('./models/user')
// var path = require('path')
// var exphbs = require('express-handlebars')
// var expressValidator = require('express-validator')
// var flash = require('connect-flash')

var passport = require('passport')
var passportLocalMongoose = require('passport-local-mongoose')
var LocalStrategy = require('passport-local')
// var cookieParser = require('cookie-parser')

// var routes = require('./routes/index')
// var users = require('./routes/users')


//Init application
var app = express()
//DB connection
mongoose.connect('mongodb://users:lifeisgreat94@ds145039.mlab.com:45039/users')
// //Bodyparse Middleware
app.use(bodyParser.urlencoded({extended: true}))
// app.use(bodyParser.json)
// app.use(cookieParser())

app.set("view engine", "ejs")
// //View engine
// app.set('views', path.join(__dirname, 'views'))
// app.engine('handlerbars', exphbs({defaultLayout:'layout'}))
// app.set('view engine', 'handlerbars')

// //Static folder
// app.use(express.static(path.join(__dirname, 'public')))

// //Express Session
app.use(require('express-session')({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
}))

//Passport init
app.use(passport.initialize())
app.use(passport.session())

passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

// //Express Validator
// app.use(expressValidator({
//     errorFormatter: function(param, msg, value){
//         const namespace = param.split('.')
//         , root = namespace.shift()
//         , formParam = root
        
//      while(namespace.length)(
//           formParam += '['+ namespace.shift() +']')
//           return {
//               param : formParam,
//               msg : msg,
//               value : value
//           }
//     }
// }))

// //Connect flash
// app.use(flash())

// //Global variables
// app.use(function(req, res, next){
//     res.locals.success_msg = req.flash('success_msg')
//     res.locals.error_msg = req.flash('error_msg')
//     res.locals.error = req.flash('error')
//     next()
// })

//Routes endpoints
// app.use('/', routes)
// app.use('/users',users)



app.get('/', function(req, res){
    
    res.render('index')
})

app.get('/secret',isLoggedIn, function(req, res){
    User.find({}, function(err, users) {
        if (err) throw err;
        // object of all the users
        console.log(users);
        res.render('secret', {users:users});
})

})

//Authentication routes
//sign up pages
app.get('/register', function(req, res){
    res.render('register')
})

//register post
app.post('/register', function(req, res){
    User.register(new User({username: req.body.username}), req.body.password, function(err, user){
        if(err){
            console.log(err)
            return res.render('register')
        }
        passport.authenticate('users')(req, res, function(){
            res.redirect('/login')
        })
    })
})
//delete a single item
app.get('/destroy/:id', function ( req, res ){
  User.findById( req.params.id, function ( error, user ){
    user.remove( function ( erorr, todo ){
      res.redirect( '/secret' );
    });
  });
})

//login render pages
app.get('/login', function(req, res){
    res.render('login')
})

//middleware
app.post('/login', passport.authenticate('local',{
    successRedirect: '/secret',
    failureRedirect: '/login'
}), function(req, res){
    
})

//logout render pages
app.get('/logout', function(req, res){
    req.logout()
    res.redirect('/')
})

//authenicate
function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}


app.listen(process.env.PORT, process.env.IP, function(){
    console.log('Success - App is running...')
})
